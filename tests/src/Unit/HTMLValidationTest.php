<?php

namespace Drupal\Tests\media_embeddable\Unit;

use Drupal\Tests\UnitTestCase;

class HTMLValidationTest extends UnitTestCase {

  /**
   * The config mock
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The config factory mock
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactoryMock;

  /**
   * The html value
   *
   * @var string
   */
  protected $html;

  /**
   * The FormState mock.
   *
   * @var \Drupal\Core\FOrm\FormStateInterface
   */
  protected $formStateMock;

  /**
   * The form errors
   *
   * @var array
   */
  protected $formErrors = [];

  /**
   * Mock function for 'getValue'
   *
   * @param string $name
   *
   * @return void
   */
  public function getFormValue($name) {
    switch ($name) {
    case 'html':
      return $this->html;
    default:
      return NULL;
    }
  }

  /**
   * Mock function for 'setErrorByName'
   *
   * @param string $name
   * @param string $error
   *
   * @return void
   */
  public function setFormErrorByName($name, $error) {
    if (!isset($this->formErrors[$name])) {
      $this->formErrors[$name] = $error;
    }
  }

  /**
   * Mock the 't' function'
   *
   * @param string $string
   * @param array $replacements
   *
   * @return void
   */
  public function t($string, $replacements = []) {
    if (!empty($replacements)) {
      foreach ($replacements as $search => $replacement) {
        $string = str_replace($search, $replacement, $string);
      }
    }

    return $string;
  }

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Mock the settings.
    $this->config = $this->getMockBuilder(\Drupal\Core\Config\Config::class)->disableOriginalConstructor()->setMethodsExcept(['get', 'set'])->getMock();

    // Mock the form state.
    $this->formStateMock = $this->getMockBuilder(\Drupal\Core\Form\FormState::class)->disableOriginalConstructor()->setMethodsExcept()->getMock();
    $this->formStateMock->method('getValue')->will($this->returnCallback([$this, 'getFormValue']));
    $this->formStateMock->method('setErrorByName')->will($this->returnCallback([$this, 'setFormErrorByName']));

    // Default config.
    $this->config->set('allowed_hosts', []);
    $this->config->set('allow_tag_without_src', TRUE);
    $this->config->set('allow_tag_with_content', TRUE);
    $this->config->set('only_allowed_hosts', FALSE);

    // Config factory mock.
    $this->configFactoryMock = $this->getMockBuilder(\Drupal\Core\Config\ConfigFactory::class)->disableOriginalConstructor()->onlyMethods(['get'])->getMock();
    $this->configFactoryMock->method('get')->willReturn($this->config);
  }

  /**
   * Tests the html validation of 'EmbeddableForm'
   *
   */
  public function testHTMLValidation() {
    $form = [];
    /** @var \Drupal\media_embeddable\Form\EmbeddableForm $html_embed_form_mock */
    $html_embed_form_mock = $this->getMockBuilder(\Drupal\media_embeddable\Form\EmbeddableForm::class)
      ->disableOriginalConstructor()
      ->setMethodsExcept(['validateHtml', 'setConfigFactory'])
      ->onlyMethods(['t'])
      ->getMock();
    $html_embed_form_mock->method('t')->will($this->returnCallback([$this, 't']));
    $this->html = '<script src="test">Test</script>';
    $html_embed_form_mock->setConfigFactory($this->configFactoryMock);
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(0, $this->formErrors);

    $this->config->set('allowed_hosts', ['instagram.com']);
    $this->formErrors = [];
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(0, $this->formErrors);

    $this->config->set('only_allowed_hosts', TRUE);
    $this->formErrors = [];
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(1, $this->formErrors);
    $this->assertTrue(str_contains($this->formErrors['html'], 'provided src'));

    $this->html = '<script src="http://www.instagram.com/somescript.js">Test</script>';
    $this->formErrors = [];
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(0, $this->formErrors);

    $this->html = '<script src="https://www.instagram.com/somescript.js">Test</script>';
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(0, $this->formErrors);

    $this->html = '<script src="https://instagram.com/somescript.js">Test</script>';
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(0, $this->formErrors);

    $this->html = '<script src="https://subdomain.instagram.com/somescript.js">Test</script>';
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(0, $this->formErrors);

    $this->config->set('allow_tag_with_content', FALSE);
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(1, $this->formErrors);
    $this->assertTrue(str_contains($this->formErrors['html'], 'should not have content'));

    $this->html = '<script src="http://www.instagram.com/somescript.js"></script>';
    $this->formErrors = [];
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(0, $this->formErrors);

    $this->config->set('allow_tag_without_src', TRUE);
    $this->config->set('allow_tag_with_content', TRUE);
    $this->html = '<script>Test</script>';
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(0, $this->formErrors);

    $this->html = '<script src="https://test.com/somescript.js">Test</script>';
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(1, $this->formErrors);
    $this->assertTrue(str_contains($this->formErrors['html'], 'provided src'));

    $this->html = '<script src="https://test.com/somescript.js">Test</script>';
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(1, $this->formErrors);
    $this->assertTrue(str_contains($this->formErrors['html'], 'provided src'));

    $this->html = '<script src="//test.com/somescript.js">Test</script>';
    $this->formErrors = [];
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(1, $this->formErrors);
    $this->assertTrue(str_contains($this->formErrors['html'], 'provided src'));

    $this->html = '<script src="test.com/somescript.js">Test</script>';
    $this->formErrors = [];
    $html_embed_form_mock->validateHtml($form, $this->formStateMock);
    $this->assertCount(1, $this->formErrors);
    $this->assertTrue(str_contains($this->formErrors['html'], 'provided src'));
  }
}