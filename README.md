# Media: Embeddable

Provides media entity to embed html code.

## Usage

- Install the module
- Configure the settings "/admin/config/media_embeddable"
- Add 'Embeddable' media entities.
