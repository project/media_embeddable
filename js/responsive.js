/**
 * @file
 */

(function ($, Drupal, once) {
  Drupal.behaviors.mediaHtmlEmbedResponsive = {
    replacePx: function (content) {
      if (typeof content === "string" && content.includes("px")) {
        return parseInt(content.replace("px", ""));
      }

      return content;
    },
    attach: function (context, settings) {
      var object = this;
      $(
        once("mediaHtmlEmbedResponsive", ".media-embeddable>iframe", context)
      ).each(function () {
        // If the iframe is a facebook one. Do not modify it.
        /** @type {string} src */
        var src = $(this).attr("src");
        if (/(\w*)?facebook.com\//.test(src)) {
          return;
        }

        var width = object.replacePx(this.width);
        var height = object.replacePx(this.height);

        if (width > 0 && height > 0) {
          var aspectRatio = height / width;
          $(this)
            .parent()
            .addClass("responsive")
            .attr(
              "style",
              "padding-bottom:" + (aspectRatio * 100).toString() + "%;"
            );
        }
      });
    },
  };
})(jQuery, Drupal, once);
