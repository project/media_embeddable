<?php

namespace Drupal\media_embeddable\Form;

use DOMDocument;
use DOMXPath;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\media_embeddable\Plugin\media\Source\HTMLEmbed;
use Drupal\media_library\Form\AddFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an embeddable form.
 *
 * @source media_entity_soundcloud
 *   Thanks for the 'media_entity_soundcloud' module for this code.
 */
class EmbeddableForm extends AddFormBase {

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMediaType(FormStateInterface $form_state) {
    if ($this->mediaType) {
      return $this->mediaType;
    }

    $media_type = parent::getMediaType($form_state);
    if (!$media_type->getSource() instanceof HTMLEmbed) {
      throw new \InvalidArgumentException('Can only add media types which use a "Media: Embeddable" source plugin.');
    }
    return $media_type;
  }

  /**
   * {@inheritDoc}
   */
  protected function buildInputElement(array $form, FormStateInterface $form_state) {
    $container = [
      '#type' => 'container',
    ];
    $container['html'] = [
      '#type' => 'textarea',
      '#title' => $this->t("HTML"),
    ];

    $container['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
      '#submit' => ['::addButtonSubmit'],
      '#validate' => ['::validateHtml'],
      '#ajax' => [
        'callback' => '::updateFormCallback',
        'wrapper' => 'media-library-wrapper',
        // @todo Remove when https://www.drupal.org/project/drupal/issues/2504115
        //   is fixed.
        'url' => Url::fromRoute('media_library.ui'),
        'options' => [
          'query' => $this->getMediaLibraryState($form_state)->all() + [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
      ],
    ];

    $form['container'] = $container;

    return $form;
  }

  /**
   * Validates the HTML code.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return void|true
   */
  public function validateHtml(array &$form, FormStateInterface $form_state) {
    $html = $form_state->getValue('html');
    $config = $this->configFactory->get('media_embeddable.settings');
    $dom = new DOMDocument();
    $result = $dom->loadHTML($html);
    if (!$result) {
      $form_state->setErrorByName('html', $this->t('Invalid HTML'));
    }

    // Check for any script tag.
    if ($result) {
      $query = new DOMXPath($dom);
      $script_tags = $query->query('//script');
      foreach ($script_tags as $script_tag) {
        /** @var \DOMElement $script_tag */
        $src = $script_tag->getAttribute('src');
        $allow_tag_without_src = (bool) $config->get('allow_tag_without_src');
        if (empty($src) && !$allow_tag_without_src) {
          $form_state->setErrorByName('html', $this->t('Script tags without soruce are not allowed'));
        }

        $allow_tag_with_content = (bool) $config->get('allow_tag_with_content');
        if ((!empty($script_tag->nodeValue) || $script_tag->childNodes->count() > 0) && !$allow_tag_with_content) {
          $form_state->setErrorByName('html', $this->t('Script tags should not have content inside them'));
        }

        // Get the allowed hosts.
        $allowed_hosts = $config->get('allowed_hosts');
        $only_allowed_hosts = (bool) $config->get('only_allowed_hosts');
        // Check if the src complied with the allowed hosts.
        $allow_host = FALSE;
        if ($src && $only_allowed_hosts) {
          foreach ($allowed_hosts as $host) {
            $host = str_replace('.', '\.', trim($host));
            $regex = "/(https?:)?\/\/(\w+\.)?$host/";
            if (preg_match($regex, $src)) {
              $allow_host = TRUE;
            }
          }
        } else {
          $allow_host = TRUE;
        }

        // If the host in the src is not allowed.
        if (!$allow_host) {
          $form_state->setErrorByName('html', $this->t('The provided src "@source" is not allowed', [
            '@source' => $src,
          ]));
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function addButtonSubmit(array $form, FormStateInterface $form_state) {
    $this->processInputValues([$form_state->getValue('html')], $form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'media_embeddable_media_form';
  }

}
