<?php

namespace Drupal\media_embeddable\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MediaEmbeddableSettings.
 */
class MediaEmbeddableSettings extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new MediaEmbeddableSettings object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($config_factory);
    $this->configFactory = $config_factory;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_embeddable.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_embeddable_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('media_embeddable.settings');

    $form['allow_tag_without_src'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow script tags without src'),
      '#default_value' => (bool) $config->get('allow_tag_without_src'),
    ];

    $form['allow_tag_with_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow script tags with content inside them'),
      '#default_value' => (bool) $config->get('allow_tag_with_content'),
    ];

    $form['only_allowed_hosts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only allow script tags with sources from certain hosts'),
      '#default_value' => (bool) $config->get('only_allowed_hosts'),
    ];

    $allowed_hosts = $config->get('allowed_hosts');
    $form['allowed_hosts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed Hosts'),
      '#default_value' => is_array($allowed_hosts) ? implode(PHP_EOL, $allowed_hosts) : "",
      '#states' => [
        'visible' => [
          '[name="only_allowed_hosts"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $allowed_sources = explode(PHP_EOL, $form_state->getValue('allowed_hosts'));
    $allowed_sources = array_map(function ($host) {
      return trim($host);
    }, $allowed_sources);

    $this->config('media_embeddable.settings')
      ->set('allow_tag_without_src', $form_state->getValue('allow_tag_without_src'))
      ->set('allow_tag_with_content', $form_state->getValue('allow_tag_with_content'))
      ->set('only_allowed_hosts', $form_state->getValue('only_allowed_hosts'))
      ->set('allowed_hosts', $allowed_sources)
      ->save();
  }

}
