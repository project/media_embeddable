<?php

namespace Drupal\media_embeddable\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Plugin implementation of the 'html_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "html_field_formatter",
 *   label = @Translation("HTML Field Formatter"),
 *   field_types = {
 *     "text_long"
 *   }
 * )
 */
class HTMLFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [
      'Render the field as HTML',
      $this->t('Responsive: @responsive', ['@responsive' => $this->getSetting('responsive') ? $this->t('enabled') : $this->t('disabled')]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'responsive' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['responsive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Responsive'),
      '#description' => $this->t('This will try to make each IFrame responsive (It must have a height and a width)'),
      '#default_value' => $this->getSetting('responsive'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => Markup::create($item->value)];
    }

    $render = [
      '#theme' => 'media_embeddable',
      'elements' => $elements,
    ];

    if ((bool) $this->getSetting('responsive')) {
      $render['#attached']['library'][] = 'media_embeddable/responsive';
    }

    return $render;
  }

}
