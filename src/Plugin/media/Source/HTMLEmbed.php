<?php

namespace Drupal\media_embeddable\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;

/**
 * HTML embed entity media source.
 *
 * @MediaSource(
 *   id = "media_embeddable",
 *   label = @Translation("Embeddable"),
 *   allowed_field_types = {"text_long"},
 *   default_thumbnail_filename = "media-embeddable.png",
 *   description = @Translation("Provides business logic and metadata for HTML media entities."),
 *   forms = {
 *     "media_library_add" = "\Drupal\media_embeddable\Form\EmbeddableForm"
 *   }
 * )
 */
class HTMLEmbed extends MediaSourceBase {

  /**
   * {@inheritDoc}
   */
  public function getMetadataAttributes() {
    return [];
  }

  /**
   *
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    switch ($attribute_name) {
    case 'default_name':
      return $media->uuid();

    default:
      return parent::getMetadata($media, $attribute_name);
    }
  }

}
